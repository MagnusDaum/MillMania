#-------------------------------------------------
#
# Project created by QtCreator 2016-08-13T08:27:43
#
# Author: Magnus Daum
#
#-------------------------------------------------

CONFIG += qt

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QMAKE_CXXFLAGS+= -openmp
QMAKE_LFLAGS +=  -openmp

TARGET = MillMania
TEMPLATE = app

SOURCES += main.cpp\
		ui/mainwindow.cpp \
		ui/canvas.cpp \
		ui/gamecoordinatorgui.cpp \
		game/gameboard.cpp \
		game/gamecoordinator.cpp \
		game/gamecoordinatorbase.cpp \
		game/aicoordinator.cpp \
		game/ai/aisimple.cpp \
		game/ai/aibacktrack.cpp \
		game/ai/aibase.cpp


HEADERS  += ui/mainwindow.h \
			ui/canvas.h \
			ui/gamecoordinatorgui.h \
			game/gameboard.h \
			game/gamecoordinator.h \
			game/gamecoordinatorbase.h \
			game/drawaction.h \
			game/aicoordinator.h \
			game/ai/aisimple.h \
			game/ai/aibase.h \
			game/ai/aibacktrack.h

FORMS    += ui/mainwindow.ui

win32:RC_FILE = $$PWD/res/millmania.rc

release:DESTDIR = $$PWD/bin/release
debug:DESTDIR = $$PWD/bin/debug
