#define _USE_MATH_DEFINES // for M_PI
#include <cmath> // for M_PI

#include "canvas.h"

#include "gamecoordinatorgui.h"

Canvas::Canvas(QWidget *parent) : QWidget(parent)
  , boardScale(NumPolygons)
{
}

void Canvas::SetCoordinator(const GameCoordinatorGUI *coordinator)
{
	this->coordinator = coordinator;

	positionsPerPolygon = coordinator->GetGameBoardPointer()->positionsPerPolygon;

	NumPolygons = static_cast<int>(coordinator->GetBoardSize() / positionsPerPolygon);

	boardScale.resize(NumPolygons);
	const int N = positionsPerPolygon / 2;
	double scaleFactor = 1;
	if(N % 2 == 0)
	{
		scaleFactor = 1/cos(M_PI/N);
	}
	for(auto i=0; i<NumPolygons; ++i)
	{
		boardScale[i] = scaleFactor * 0.45 * (i+1) / NumPolygons;
	}

	resizeEvent(0);

	FillBoardToCanvasMapping();

	this->update();
}

void Canvas::SetPlayerNames(const std::vector<QString>* playerNames)
{
	this->playerNames = playerNames;
}

void Canvas::SetPaintAssistance(bool paintAssistanceOn)
{
	this->paintAssistanceState = paintAssistanceOn;
}

void Canvas::paintEvent(QPaintEvent*)
{
    QPainter p(this);

	// Draw Border of canvas
	p.drawRect(0,0,this->width()-1,this->height()-1);

	int linewidth = 3;
	p.setPen(QPen(Qt::black, linewidth));

	// Draw text in top left corner
	int text_x = 10;

	p.drawText(QPoint(text_x,20), coordinator->GetGameState() == MillGame::GameState::GAME_OVER ? labelStrings[5] : labelStrings[4]);

	auto playerIndex = coordinator->GetActivePlayer();

	QString playerName = playerNames->at(playerIndex);

	// Some hard coding here to check if you have to show playerIndex (i.e. in case human vs human)
	if(playerNames->at(1).compare(playerNames->at(2)) == 0)
	{
		playerName.append(" " + QString::number(playerIndex));
	}

	p.drawText(QPoint(text_x,40), playerName);

	p.drawText(QPoint(text_x,90), labelStrings[6]);
	p.drawText(QPoint(text_x,110), labelStrings[static_cast<int>(coordinator->GetGameState())+1]);

	if(coordinator->GetGameState() == MillGame::GameState::PLACE)
	{
		p.drawText(QPoint(text_x,160), labelStrings[7]);
		p.drawText(QPoint(text_x,180),QString::number(coordinator->GetRemainingTokensToPlace(coordinator->GetActivePlayer())));
	}

	// Paint the actual board, including all tokens
	paintBoard(p);

	if(coordinator->GetGameState() == MillGame::GameState::GAME_OVER)
	{
		// Draw "Game Over" in middle of board
		QFont f = p.font();
		f.setPointSize(60);
		p.setFont(f);

		const float size = 200;
		QPointF corner(this->width()/2 - size/2, this->height()/2 - size/2);
		QRectF rect(corner, QSizeF(size, size));
		p.drawText(rect, labelStrings[static_cast<int>(coordinator->GetGameState()) + 1]);
	}
}

void Canvas::paintBoard(QPainter& painter)
{
	painter.setPen(QPen(Qt::black, lineWidth));

	// Draw polygons
	const int N = positionsPerPolygon / 2; // regular N-polygon has 2*N token positions
	for(auto i = 0; i<NumPolygons; ++i)
	{
		for(auto n = 0; n<positionsPerPolygon-1; ++n)
		{
			painter.drawLine(	boardCanvasPositions[i*positionsPerPolygon + n],
								boardCanvasPositions[i*positionsPerPolygon + n + 1]);
		}
		painter.drawLine(	boardCanvasPositions[i*positionsPerPolygon],
							boardCanvasPositions[(i+1)*positionsPerPolygon - 1]);
	}
	// Connect polygons
	for(auto i=1; i<positionsPerPolygon; i+=2) // only odd positions -> edges
	{
		painter.drawLine( boardCanvasPositions[ i ],
						  boardCanvasPositions[ i + (NumPolygons-1)*positionsPerPolygon ]);
	}

	// Draw Tokens
	if(coordinator)
	{
		if(paintAssistanceState)
		{
			paintAssistance(painter, curMovingToken);
		}

		for(auto i=0; i<coordinator->GetBoardSize(); ++i)
		{
			if(i == curMovingToken)
			{
				// dont draw currently moving token on normal board position
				continue;
			}

			auto val = coordinator->GetBoardValue(i);
			if(val > 0)
			{
				Qt::GlobalColor color = tokenColors.count(val) ? tokenColors.at(val) : Qt::black;
				//p.setPen(QPen(color, linewidth));
				painter.setBrush(color);

				QPointF center = mapToCanvasCoordinates(i);

				painter.drawEllipse(center,tokenRadius,tokenRadius);
			}

			//painter.drawText(mapToCanvasCoordinates(i),QString::number(i)); // show board numbers for debugging
		}

		//Draw Active/Moving Token
		if(curMovingToken >= 0)
		{
			auto val = coordinator->GetBoardValue(curMovingToken);
			Qt::GlobalColor color = tokenColors.count(val) ? tokenColors.at(val) : Qt::black;
			painter.setBrush(color);
			painter.drawEllipse(curMovingTokenPos,tokenRadius,tokenRadius);
		}
	}
}

void Canvas::paintAssistance(QPainter& painter, int movingToken)
// Highlights possible player draws
// i.e. which tokens can be removed, or where token can be moved
{
	auto gameState = coordinator->GetGameState();
	// Highlight possible removes
	if(gameState == MillGame::GameState::REMOVE)
	{
		painter.setPen(QPen(possibleRemoveColor, lineWidth));
		painter.setBrush(possibleRemoveColor);
		auto posdraws = coordinator->GetPossibleDraws();
		for(auto& draw : posdraws)
		{
			int pos = draw.position;
			QPointF center = mapToCanvasCoordinates(pos);
			painter.drawEllipse(center,tokenRadius*1.1,tokenRadius*1.1);
		}
		painter.setPen(QPen(Qt::black, lineWidth));
	}
	// highlight possible moves
	else if(movingToken >=0)
	{
		painter.setPen(QPen(possibleMoveColor, lineWidth));
		painter.setBrush(possibleMoveColor);
		auto moves = coordinator->GetGameBoardPointer()->GetPossibleMoves(
						movingToken,
						coordinator->CanPlayerJump(coordinator->GetActivePlayer()));
		for(int pos : moves)
		{
			QPointF center = mapToCanvasCoordinates(pos);
			painter.drawEllipse(center,tokenRadius*0.2,tokenRadius*0.2);
		}
		painter.setPen(QPen(Qt::black, lineWidth));
	}
}

void Canvas::mousePressEvent(QMouseEvent *event)
// This is only used to set currently moving token
// actual draw action is send in MouseReleaseEvent
{
	if(!coordinator) return;

	Qt::MouseButton mb = event->button();
	if(mb == Qt::LeftButton)
	{
		int boardPos = mapToBoardPosition(event->pos(),tokenRadius*1.5);
		if ( coordinator->CanMoveToken(boardPos) )
		{
			curMovingToken = boardPos;
			curMovingTokenPos = event->pos();
			this->update();
		}
	}
}

void Canvas::mouseReleaseEvent(QMouseEvent *event)
// sends draw action according to user input
{
	if(!coordinator) return;

	Qt::MouseButton mb = event->button();
	if(mb == Qt::LeftButton)
	{
		int boardPos = mapToBoardPosition(event->pos(),tokenRadius*1.5);
		if(boardPos >= 0)
		{
			if(curMovingToken >= 0)
			{
				//update beforehand, otherwise there will be a ghost token, if ai is thinking long
				int tokenIdx = curMovingToken;
				curMovingToken = -1;
				this->update();
				// moving
				emit userDrawSignal(MillGame::DrawAction(MillGame::DrawType::MOVE,tokenIdx,boardPos));
			}
			else
			{
				if(coordinator->GetBoardValue(boardPos) == 0)
				{
					// placing
					emit userDrawSignal(MillGame::DrawAction(MillGame::DrawType::PLACE,boardPos));
				}
				else if(coordinator->GetBoardValue(boardPos) != coordinator->GetActivePlayer())
				{
					// removing
					emit userDrawSignal(MillGame::DrawAction(MillGame::DrawType::REMOVE,boardPos));
				}
			}
		}
	}

	// Draw no more moving token
	curMovingToken = -1;

	this->update();
}

void Canvas::mouseMoveEvent(QMouseEvent *event)
// draw moving token accordingly
{
	if(curMovingToken >= 0)
	{
		curMovingTokenPos = event->pos();
		this->update();
	}
}

void Canvas::resizeEvent(QResizeEvent *)
// fit board size to canvas size
{
	boardLength = static_cast<int>(std::min(this->width(),this->height()));

	tokenRadius = boardLength / 25;

	if(positionsPerPolygon == 6)
	{
		// tokens are too close for a triangle board
		tokenRadius *= 0.6;
	}
	else
	{
		// tokens are too close for polygons with many edges
		// but if they're too small you can't click them either
		tokenRadius *= std::max(pow(0.95, positionsPerPolygon-10), 0.15);
	}

	FillBoardToCanvasMapping();
}

int Canvas::mapToBoardPosition(QPointF MousePos, double radius) const
// not the most efficient solution, but it works and was easy to code
// basically map each board position to a canvas postion
// and see if user clicked in range of the position (hence radius parameter)
{
	for(auto i = 0; i<coordinator->GetBoardSize(); ++i)
	{
		auto pt = mapToCanvasCoordinates(i);
		if( sqrt(pow(MousePos.x()-pt.x(),2) + pow(MousePos.y()-pt.y(),2)) < radius )
		{
			return i;
		}
	}
	return -1;
}

QPointF Canvas::mapToCanvasCoordinates(int BoardPos) const
// Mapping is precomputed at each canvas size change
{
	return boardCanvasPositions[BoardPos];
}

void Canvas::FillBoardToCanvasMapping()
// Calculate the mapping from board position to canvas
{
	boardCanvasPositions.clear();

	const int N = positionsPerPolygon / 2; // regular N-polygon has 2*N token positions

	// angleOffset makes sure that each bottom edge of every board is horizontal
	double angleOffset;
	if(N % 2 == 0)
		angleOffset = M_PI/N;
	else
		angleOffset = 0;

	for(auto i = 0; i<NumPolygons; ++i)
	{
		int sideLength = boardLength * boardScale[i];

		// Corners of polygons are easily mappend
		std::vector<QPoint> polygonPoints;
		for(auto n = 0; n<N; ++n)
		{
			QPoint p;
			p.setX( this->width() /2 + sideLength * sin(2*M_PI*n/N + angleOffset) );
			p.setY( this->height()/2 - sideLength * cos(2*M_PI*n/N + angleOffset) );
			polygonPoints.push_back(p);
		}

		// for centers of edges use middle of the neighboring polygon corners
		std::vector<QPoint> edgePoints;
		for(auto n = 0; n < N-1; ++n)
		{
			QPoint edgeCenterPoint = (polygonPoints[n] + polygonPoints[n+1])/2;
			edgePoints.push_back(edgeCenterPoint);
		}
		edgePoints.push_back( (polygonPoints[0] + polygonPoints[N-1])/2 );

		// now push points back in distinct order
		for(auto n = 0; n < N; ++n)
		{
			boardCanvasPositions.push_back(polygonPoints[n]);
			boardCanvasPositions.push_back(edgePoints[n]);
		}
	}
}
