#include "gamecoordinatorgui.h"

#include <chrono>
#include <thread>

GameCoordinatorGUI::GameCoordinatorGUI(QWidget*, int positionsPerBoardLayer)
	: GameCoordinator(positionsPerBoardLayer, MillGame::GameMode::DEFAULT, 1)
{
}

void GameCoordinatorGUI::DoAIDraw()
// Overrides AI Draw to update gui accordingly
// Furthermore wait some time, so user can actually see changes and they dont appear instantly
{
	emit updateGUI();

	auto start = std::chrono::system_clock::now();

	GameCoordinator::DoAIDraw();

	auto end = std::chrono::system_clock::now();

	auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);

	if(elapsed.count() < minimalAIDrawTime)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(minimalAIDrawTime - elapsed.count()));
	}
}
