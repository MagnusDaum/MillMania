#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <memory>

#include "game/drawaction.h"
#include "gamecoordinatorgui.h"

namespace Ui {
class MainWindow;
}

// This window is more or less the middleman between Canvas GUI and Game Coordinator/Engine
// But is has several widgets for controlling game variants
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
	void uiUpdateFunction();
	void userInputSlot(MillGame::DrawAction draw);

private slots:
	void on_resetBtn_released();

	void on_player2ComboBox_currentIndexChanged(int index);

	void on_npcDifficulty1SBox_valueChanged(int arg1);

	void on_player1ComboBox_currentIndexChanged(int index);

	void on_npcDifficulty2SBox_valueChanged(int arg1);

	void on_undoBtn_released();

	void on_polygonSB_valueChanged(int arg1);

	void on_actionLasker_triggered();

	void on_actionDefault_triggered();

private:
	void closeEvent(QCloseEvent *event);

    Ui::MainWindow *ui;

	std::unique_ptr<GameCoordinatorGUI> coordinator;

	const std::vector<QString> aiStrings =
	{
		"Human"
		, "AI Simple"
		, "AI Backtrack"
	};

	static const int playerCount = 2;

	std::vector<QString> playerNames;
};

#endif // MAINWINDOW_H
