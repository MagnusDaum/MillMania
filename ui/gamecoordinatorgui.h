#ifndef GAMECOORDINATORGUI_H
#define GAMECOORDINATORGUI_H

#include "game/gamecoordinator.h"
#include <QObject>

// Inherit from normal Game Coordinator
// this class emits signals to update gui after ai draws
// (this is necessary so ai vs ai games have visible turns)
// Furthermore waits after every ai turn, so drawing is not done instantly after player turn, if ai is fast
class GameCoordinatorGUI : public QObject, public MillGame::GameCoordinator
{
	Q_OBJECT

public:
	GameCoordinatorGUI(QWidget*, int positionsPerBoardLayer);

signals:
	void updateGUI();

private:
	void DoAIDraw();

	// if AI is too fast, wait before updating gui, so user sees changes
	const int minimalAIDrawTime = 500; // milliseconds
};

#endif // GAMECOORDINATORGUI_H
