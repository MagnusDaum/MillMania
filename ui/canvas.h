#ifndef CANVAS_H
#define CANVAS_H

#include <QWidget>
#include <QPainter>
#include <QMouseEvent>
#include <QResizeEvent>

#include "game/drawaction.h"

// Forward declarations
class GameCoordinatorGUI;

class Canvas : public QWidget
{
    Q_OBJECT
public:
    explicit Canvas(QWidget *parent = 0);

	// Event handling
    virtual void paintEvent(QPaintEvent*);
	virtual void mousePressEvent(QMouseEvent *event);
	virtual void mouseReleaseEvent(QMouseEvent *event);
	virtual void mouseMoveEvent(QMouseEvent *event);
	virtual void resizeEvent(QResizeEvent *);

	// Configure Canvas even after constructor
	void SetCoordinator(const GameCoordinatorGUI *coordinator);
	void SetPlayerNames(const std::vector<QString>* playerNames);
	void SetPaintAssistance(bool paintAssistanceOn);

signals:
	// emit this is user inputs a draw request via gui
	void userDrawSignal(MillGame::DrawAction draw);

private:
	void paintBoard(QPainter& painter);
	void paintAssistance(QPainter& painter, int movingToken = -1);

	void FillBoardToCanvasMapping();

	int mapToBoardPosition(QPointF MousePos, double radius) const;
	QPointF mapToCanvasCoordinates(int BoardPos) const;

	const int lineWidth = 3;

	const std::map< int, Qt::GlobalColor > tokenColors =
	{
		{ 1, Qt::white },
		{ 2, Qt::black }
	};

	const Qt::GlobalColor possibleRemoveColor = Qt::red;
	const Qt::GlobalColor possibleMoveColor = Qt::green;

	const std::vector<QString> labelStrings =
	{
		"Game Over"
		, "Place Token"
		, "Move Token"
		, "Remove Token"
		, "Active Player:"
		, "Winner:"
		, "Active Phase:"
		, "Left to place:"
	};

	const GameCoordinatorGUI *coordinator;

	// Get player names from mainwindow
	const std::vector<QString>* playerNames;

	// mapping for board position to canvas
	// create on resize and only read from in paint event
	std::vector<QPointF> boardCanvasPositions;

	// variables for drawing board
	int boardLength;
	int tokenRadius;
	int NumPolygons = 3; // will be taken from gamecoordinator
	int positionsPerPolygon = 8; // will be taken from gamecoordinator
	std::vector<double> boardScale;

	// for drawing tokens
	int curMovingToken = -1;
	QPoint curMovingTokenPos;
	bool paintAssistanceState = true;	
};

#endif // CANVAS_H
