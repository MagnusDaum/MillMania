#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QSizePolicy>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
  ,playerNames(playerCount+1)
{	
    ui->setupUi(this);

	// set difficulty selection to use space even when player is human
	// -> no widget moving on player switch
	QSizePolicy sPoli = ui->npcDifficulty1Lab->sizePolicy();
	sPoli.setRetainSizeWhenHidden(true);
	ui->npcDifficulty1Lab->setSizePolicy(sPoli);
	sPoli = ui->npcDifficulty2Lab->sizePolicy();
	sPoli.setRetainSizeWhenHidden(true);
	ui->npcDifficulty2Lab->setSizePolicy(sPoli);

	ui->canvas->setSizePolicy(QSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding));

	coordinator = std::unique_ptr<GameCoordinatorGUI>(new GameCoordinatorGUI(this, 8));
	connect(coordinator.get(), SIGNAL(updateGUI()), this, SLOT(uiUpdateFunction()));
	ui->canvas->SetCoordinator(coordinator.get());

	for(auto &s : aiStrings)
	{
		ui->player1ComboBox->addItem(s);
		ui->player2ComboBox->addItem(s);
	}

	// default ai is backtracking with difficulty (=depth) three
	ui->player2ComboBox->setCurrentIndex(2);
	ui->npcDifficulty2SBox->setValue(3);

	playerNames.at(0) = "Tie";
	playerNames.at(1) = ui->player1ComboBox->currentText();
	playerNames.at(2) = ui->player2ComboBox->currentText();

	ui->canvas->SetPlayerNames(&playerNames);

	connect(ui->canvas, SIGNAL(userDrawSignal(MillGame::DrawAction)), this, SLOT(userInputSlot(MillGame::DrawAction)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
// OpenMP won't stop if window is closed
// and I didn't have the time to develop a clean fix
// --> prevent closing while AI is running
{
	if(coordinator->GetIsAIFinished())
	{
		event->accept();
	}
	else
	{
		event->ignore();
	}
}

void MainWindow::uiUpdateFunction()
{
	ui->canvas->update();
	QApplication::processEvents();
}

void MainWindow::userInputSlot(MillGame::DrawAction draw)
{
	coordinator->RequestDrawAction(draw);
}

void MainWindow::on_resetBtn_released()
// Reset Game
{
	coordinator->ResetGame(coordinator->GetMinPlayerIndex());
	ui->canvas->update();
}

void MainWindow::on_player1ComboBox_currentIndexChanged(int index)
// Change player 1
{
	playerNames.at(1) = ui->player1ComboBox->currentText();
	coordinator->ChangePlayerType(1, index, ui->npcDifficulty1SBox->value());
	if(index > 0)
	{
		ui->npcDifficulty1SBox->show();
		ui->npcDifficulty1Lab->show();
	}
	else
	{
		ui->npcDifficulty1SBox->hide();
		ui->npcDifficulty1Lab->hide();
	}
	ui->canvas->update();
}

void MainWindow::on_player2ComboBox_currentIndexChanged(int index)
// Change player 2
{
	playerNames.at(2) = ui->player2ComboBox->currentText();
	coordinator->ChangePlayerType(2, index, ui->npcDifficulty2SBox->value());
	if(index > 0)
	{
		ui->npcDifficulty2SBox->show();
		ui->npcDifficulty2Lab->show();
	}
	else
	{
		ui->npcDifficulty2SBox->hide();
		ui->npcDifficulty2Lab->hide();
	}
	ui->canvas->update();
}

void MainWindow::on_npcDifficulty1SBox_valueChanged(int arg1)
// Change Player 1 difficulty
{
	coordinator->ChangePlayerType(1, ui->player1ComboBox->currentIndex(), arg1);
	ui->canvas->update();
}

void MainWindow::on_npcDifficulty2SBox_valueChanged(int arg1)
// Change Player 2 difficulty
{
	coordinator->ChangePlayerType(2, ui->player2ComboBox->currentIndex(), arg1);
	ui->canvas->update();
}

void MainWindow::on_undoBtn_released()
// Undo draw/turn
// for two humas: only one draw
// if ai is present: two full turns (e.g. move + remove is one full turn, but two draws)
{
	int curPlayer = coordinator->GetActivePlayer();
	if(coordinator->GetPlayerType(curPlayer) == 0)
	{
		int types = coordinator->GetPlayerType(1) + coordinator->GetPlayerType(2); // 0 -> both human, else at least one ai
		if(types == 0)
		{
			// both human
			coordinator->UndoPreviousDraw();
		}
		else
		{
			// at least one ai
			coordinator->UndoPreviousTurns(2);
		}
	}
	ui->canvas->update();
}

void MainWindow::on_polygonSB_valueChanged(int arg1)
// Change board layout -> create new Game Coordinator
{
	if(coordinator->GetIsAIFinished())
	{
		coordinator = std::unique_ptr<GameCoordinatorGUI>(new GameCoordinatorGUI(this, 2*arg1));
		coordinator->ChangePlayerType(1, ui->player1ComboBox->currentIndex(), ui->npcDifficulty1SBox->value());
		coordinator->ChangePlayerType(2, ui->player2ComboBox->currentIndex(), ui->npcDifficulty2SBox->value());
		connect(coordinator.get(), SIGNAL(updateGUI()), this, SLOT(uiUpdateFunction()));
		ui->canvas->SetCoordinator(coordinator.get());
	}
}

void MainWindow::on_actionLasker_triggered()
// Switch GameMode to Lasker
{
	if(coordinator->GetIsAIFinished())
	{
		coordinator->ChangeGameMode(MillGame::GameMode::LASKER);
		ui->actionDefault->setChecked(false);
		ui->canvas->update();
	}
	else
	{
		ui->actionLasker->setChecked(false);
	}
}

void MainWindow::on_actionDefault_triggered()
// Switch GameMode to normal/default
{
	if(coordinator->GetIsAIFinished())
	{
		coordinator->ChangeGameMode(MillGame::GameMode::DEFAULT);
		ui->actionLasker->setChecked(false);
		ui->canvas->update();
	}
	else
	{
		ui->actionDefault->setChecked(false);
	}
}
