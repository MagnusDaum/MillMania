Readme for Millmania

This software was developed with C++ and QT 5.6 on Windows 10 64bit
Development environment was QtCreator 4
Compiler was Microsoft Visual C++ 14.0 (x64)

Directory structure:
(for such a small software, no separation between source and headers is made)
/game	: source/header files for game itself
/res	: resources
/ui	: source/header files for Qt GUI

---------------------------------------------------------------------

The software itself is a simple version of board game Mill (Nine Men's Morris)

The game is directly playable in default settings as soon as you start the executable
On the left is shown, whose turn it is and what he has to do
Placing and Removing tokens is by simply clicking on the board, Moving is by dragging a token to your desired position
Player 1 is white and Player 2 is black
Player can be changed between turns (including their difficulty)
Board and game mode changes lead to a reset of current game

Features:
- Fully functional normal and Lasker mill game mode
- Players: human vs human, human vs ai and even ai vs ai
- The game can be played on a normal mill board or a board with any regular polygon shape (e.g. pentagon or dodekagon)
- Any amount of turns can be "undone" i.e. you could for example replay the last 5 turns of a lost game
- There are two types of AIs with different difficulties:
	a simple AI which draws random (difficulty = 0), prevents enemy mills (difficulty = 1) or even closes mills (difficulty >= 2)
	a "backtrack" AI which calculates recursively its optimal draws (and uses a heuristic at depth = 0)
		difficulty is equal to recursion depth in this case
		multithreading is implemented with OpenMP, but as it is simple brute force high difficulties take a long time
		rule of thumb:	0-3 is nearly instant (hence a small pause is built in)
		(for 4 cores)	4 takes a few seconds
						from 5 on you have to wait some time
						if one player can jump, calculation time increases noticeably as there are many more options
						furthermore boards with many polygon sides or games with Lasker mode take longer
		caution: no inputs are processed while AI is calculating (be careful with ai vs ai game)