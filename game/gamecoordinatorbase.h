#ifndef GAMECOORDINATORBASE_H
#define GAMECOORDINATORBASE_H

#include <vector>

#include "gameboard.h"

namespace MillGame
{

enum class GameState {GAME_OVER = -1, PLACE = 0, MOVE = 1, REMOVE = 2};

enum class GameMode {DEFAULT = 0, LASKER = 1};

/*
 * Base Class of Game engine
 * handles every interaction with game board
 * manages active player and game state
 * provides utility functions for gui/ai
 * has itself no interaction with gui and/or ai
 */
class GameCoordinatorBase
{
public:
	GameCoordinatorBase(int positionsPerBoardLayer, GameMode gameMode, int startPlayer);
	~GameCoordinatorBase() = default;

	virtual void ResetGame(int startPlayer);

	virtual void ChangeGameMode(GameMode gameMode);

	bool DoDrawAction(DrawAction draw);

	//Undoing a RemoveAction only works for games with two players, as no information about value before removal is saved
	virtual void UndoPreviousDraw();

	// Checks if active player can move token at position
	bool CanMoveToken(int position) const;

	// Checks if player can jump (i.e. has min token count)
	bool CanPlayerJump(int playerIndex) const;

	// Several utility functions
	std::vector<DrawAction> GetPossibleDraws() const;
	int GetBoardValue(int position) const;
	std::size_t GetBoardSize() const;
	int GetRemainingTokensToPlace(int playerIndex) const;
	int GetTokensPlaced(int playerIndex) const;
	int GetActivePlayer() const;
	GameState GetGameState() const;
	const GameBoard* GetGameBoardPointer() const;
	int GetPlayerCount() const;
	int GetMinPlayerIndex() const;
	int GetMaxPlayerIndex() const;

	// Get number of already executed turns since game start
	std::size_t GetNumberOfTurns() const;

protected:
	void DetermineGameState(GameState& gameState);

	// Switches the currently activePlayer
	// can switch backwards for undoing turns
	virtual void SwitchPlayer(bool forwards = true);

	// don't use more than 2 players, or undoing won't work correctly for removing
	const int playerCount = 2;

	// this must be greater than 0, as 0 is an empty position
	const int minPlayerIndex = 1;

	const int maxPlayerIndex = minPlayerIndex + playerCount - 1;

	GameBoard board;

	// Class-internal datatype
	// This is used to save turns and thus enable "undoing" a draw/turn
	struct TurnState
	{
		TurnState(DrawAction draw, GameState gameState, int activePlayer) : draw(draw), gameState(gameState), activePlayer(activePlayer) {}

		DrawAction draw;
		GameState gameState;
		int activePlayer;
	};

	// vector of all executed turns
	std::vector<TurnState> previousTurns;

	GameState gameState = GameState::PLACE;

	GameMode gameMode = GameMode::DEFAULT;

	// counts tokens every player has placed
	// used for checking if player can place or has to move
	std::vector<int> tokensPlacedPerPlayer;

	int activePlayer = minPlayerIndex;
};

}

#endif // GAMECOORDINATORBASE_H
