#ifndef GAMEBOARD_H
#define GAMEBOARD_H

#include <map>
#include <array>
#include <vector>
#include <utility>

#include "drawaction.h"

namespace MillGame
{

// Class managing the mill board itself

// The layout of the board (for the standard case) is as following:
/*
 * 22       23      16
 *    14    15    8
 *       6  7  0
 * 21 13 5     1  9 17
 *       4  3  2
 *    12    11    10
 * 20       19      18
*/
class GameBoard
{
public:
	GameBoard(int positionsPerPolygon);
	~GameBoard() = default;

	void Reset();

	// returns: first: if draw was successful; second: if mill was closed by draw
	std::pair<bool,bool> DoDrawAction(int player, DrawAction draw);

	void UndoDrawAction(int previousPlayer, DrawAction draw);

	bool IsMovesPossibleForPlayer(int player) const;

	bool IsRemovePossible(int player, int position) const;

	// return token at position
	int GetValue(int position) const;

	// returns size of board
	size_t GetSize() const;

	// returns vector of all positions where a token could be placed
	std::vector<int> GetPossiblePlacements() const;

	// returns vector of all positions to where token at "position" could be moved
	std::vector<int> GetPossibleMoves(int position, bool canJump = false) const;

	// returns vector of all positions that could have their token removed by player
	std::vector<int> GetPossibleRemoves(int player) const;

	// returns connectivity for every position
	std::vector<int> GetNeighbors(int position) const;

	// how many tokens has player *currently* placed on board
	int GetTokenCount(int player) const;

	// Number of polygons (i.e. layers (in standard case squares)) amounting to the board
	const static int NumPolygons = 3;

	// shape of each polygon (each polygon has half as many edges/corners as positions)
	const int positionsPerPolygon;

	// boardSize is NumPolygons * positionsPerPolygon
	const int boardSize;

	// min/max tokens are always for a single player
	const int maxTokenCount; // if reached -> no more placing
	const int minTokenCount; // if less -> loss

	// Get all positions having a token which is part of a mill of a player
	std::vector< std::array<int, NumPolygons> > GetMillsOfPlayer(int player) const;

	// returns vector of all possible mills which would contain token at "position"
	// each vector element is an array of all positions forming a specific possible mill
	std::vector< std::array<int, NumPolygons> > GetAllPossibleMills(int position) const;

private:

	// return: first: if successful second: if mill was created
	std::pair<bool,bool> PlaceToken(int player, int position);

	// return: first: if successful second: if mill was created
	std::pair<bool,bool> MoveToken(int player, int currentPos, int newPos);

	// return: if removal was successful
	bool RemoveToken(int player, int position);

	// Calculate possible moves for every position on the board and save it in MoveMap
	void FillMoveMap();

	// Calculate mills for every position on the board and save it
	void FillMillVectors();

	// Checks if position is part of mill of player
	bool IsPartOfMill(int player, int position) const;

	// vector representing the board
	// for structure see top comment
	std::vector<int> board;

	// a mapping of possible moves for every position
	// generated at start up
	std::map< int, std::vector<int> > MoveMap;

	// vector with one element for every position
	// every element of this vector is another vector containing all mills for that position
	// every mill is an array of "NumSquares" positions forming a mill
	std::vector < std::vector< std::array<int, NumPolygons> > > MillsPerPositionVec;

	// list all possible mills for the board
	std::vector< std::array<int, NumPolygons> > MillVec;
};

}

#endif // GAMEBOARD_H
