#include "gamecoordinator.h"

namespace MillGame
{

GameCoordinator::GameCoordinator(int positionsPerBoardLayer, GameMode gameMode, int startPlayer)
	: GameCoordinatorBase(positionsPerBoardLayer, gameMode, startPlayer)
	, playerTypes(playerCount, 0)
	, playerDifficulties(playerCount, 0)
{
}

void GameCoordinator::ResetGame(int startPlayer)
{
	if(!isAIFinished) return;

	GameCoordinatorBase::ResetGame(startPlayer);

	CallAIIfNecessary();
}

void GameCoordinator::ChangeGameMode(GameMode gameMode)
{
	if(!isAIFinished) return;

	GameCoordinatorBase::ChangeGameMode(gameMode);
}

bool GameCoordinator::RequestDrawAction(DrawAction draw)
{
	if(!isAIFinished) return false;

	if(activePlayer >= minPlayerIndex && activePlayer <= maxPlayerIndex)
	{
		if(playerTypes[activePlayer-minPlayerIndex] == 0)
		{
			return DoDrawAction(draw);
		}
	}
	return false;
}

void GameCoordinator::UndoPreviousDraw()
{
	if(!isAIFinished) return;

	GameCoordinatorBase::UndoPreviousDraw();

	CallAIIfNecessary();
}

void GameCoordinator::UndoPreviousTurns(int NumberOfTurns)
{
	if(!isAIFinished) return;

	int curPlayer = activePlayer;
	while(NumberOfTurns > 0)
	{
		GameCoordinatorBase::UndoPreviousDraw();

		if(activePlayer != curPlayer)
		{
			curPlayer = activePlayer;
			NumberOfTurns--;
		}
	}

	CallAIIfNecessary();
}

bool GameCoordinator::ChangePlayerType(int playerIndex, int newPlayerType, int newPlayerDifficulty)
{
	if(!isAIFinished) return false;

	if(playerIndex >= minPlayerIndex && playerIndex <= maxPlayerIndex)
	{
		playerTypes[playerIndex - minPlayerIndex] = newPlayerType;
		playerDifficulties[playerIndex - minPlayerIndex] = newPlayerDifficulty;
		CallAIIfNecessary();
		return true;
	}
	return false;
}

int GameCoordinator::GetPlayerType(int index) const
{
	index -= minPlayerIndex;
	if(index >= 0 && index < playerTypes.size())
	{
		return playerTypes[index];
	}
	return -1;
}

bool GameCoordinator::GetIsAIFinished() const
{
	return isAIFinished;
}

void GameCoordinator::SwitchPlayer(bool forwards)
{
	GameCoordinatorBase::SwitchPlayer(forwards);

	CallAIIfNecessary();
}

void GameCoordinator::CallAIIfNecessary()
{
	while( activePlayer >= minPlayerIndex && activePlayer <= maxPlayerIndex &&
		   playerTypes[activePlayer-minPlayerIndex] > 0 && gameState != GameState::GAME_OVER)
	{
		if(previousTurns.size() > maxTurns)
		{
			activePlayer = minPlayerIndex - 1;
			gameState = GameState::GAME_OVER;
			return;
		}
		isAIFinished = false;
		DoAIDraw();
	}
	isAIFinished = true;
}

void GameCoordinator::DoAIDraw()
{
	// do AI action
	auto aiAction = aiCoordinator.DetermineDraw(static_cast<GameCoordinatorBase*>(this),
												playerTypes.at(activePlayer-minPlayerIndex),
												playerDifficulties.at(activePlayer-minPlayerIndex));
	if( ! DoDrawAction(aiAction) )
	{
		// AI is not working correctly
		// default win for previous player
		if(--activePlayer < minPlayerIndex)
		{
			activePlayer = maxPlayerIndex;
		}
		gameState = GameState::GAME_OVER;
	}
}

}
