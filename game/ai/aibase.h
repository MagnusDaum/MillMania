#ifndef AIBASE_H
#define AIBASE_H

#include "../gamecoordinatorbase.h"

namespace MillGame
{

namespace AI
{

class AIBase
{
public:
	// "main function", this is called by game engine
	// -> every child ai has to override this function
	virtual DrawAction DetermineDraw(const GameCoordinatorBase *coordinator, int difficulty = 0) const = 0;
protected:
	// places tokens randomly
	virtual DrawAction DrawDefault(const GameBoard *board, int playerIndex, GameState gameState) const;
};

}
}

#endif // AIBASE_H
