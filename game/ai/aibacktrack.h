#ifndef AIBACKTRACK_H
#define AIBACKTRACK_H

#include <vector>

#include "aibase.h"

namespace MillGame
{

namespace AI
{

class AIBacktrack : AIBase
{
	typedef int GameValue; // just for convenience if I want to switch later on

public:
	// "main function", this is called by game engine
	DrawAction DetermineDraw(const GameCoordinatorBase* coordinator, int difficulty = 0) const;

private:
	// Final iteration of ai
	DrawAction Draw_TrueBacktrack(GameCoordinatorBase* coordinator, int BacktrackDepth) const;
	// returns: first: value for activePlayer, second: value for playerIndex
	std::vector<GameValue> Draw_TrueBacktrack_Evaluator(GameCoordinatorBase* coordinator, const std::vector<int>& playerIndices, int BacktrackDepth) const;

	// First iteration of ai, deprecated
	DrawAction Draw_PseudoBacktrack(GameCoordinatorBase* coordinator, int BacktrackDepth) const;

	// Second iteration of ai, deprecated
	DrawAction Draw_TrueBacktrack_MinimizingEnemy(GameCoordinatorBase* coordinator, int BacktrackDepth) const;
	GameValue Draw_TrueBacktrack_Evaluator_MinimizingEnemy(GameCoordinatorBase* coordinator, int playerIndex, int BacktrackDepth) const;

	// Heuristic for Backtracking
	GameValue CalculateGameValue(const GameCoordinatorBase* coordinator, int playerIndex) const;

	// Values for board evaluating heuristic
	const GameValue winValue = 100000; // finishing the game should assign a value higher than otherwise reachable
	const GameValue turnValue = 100; // This values closing mills, as it's player's turn again
	const GameValue millValue = 30; // A closed mill is still good, as enemy can't easily remove a token from it
	const GameValue millEnemyValue = 25; // own mills are better than enemy mills
	const GameValue tokenValue = 20; // more tokens is better obviously
	const GameValue moveValue = 1; // for every move you can do the game looks better
};


}
}

#endif // AIBACKTRACK_H
