#include "aibacktrack.h"

#include <algorithm>
#include <limits>
#include <random>

#include <omp.h>

// This is using a dirty fix for a non responsive UI
// process events in sub-loops of backtrack ai
// I didn't have the time to put GUI in a separate thread
#include <QApplication>


namespace MillGame
{

namespace AI
{

DrawAction AIBacktrack::DetermineDraw(const GameCoordinatorBase *coordinator, int difficulty) const
{
	DrawAction draw;

	GameCoordinatorBase coordinatorCopy = *coordinator;
	draw = Draw_TrueBacktrack(&coordinatorCopy, difficulty);

	return draw;
}

AIBacktrack::GameValue AIBacktrack::CalculateGameValue(const GameCoordinatorBase* coordinator, int playerIndex) const
{
	GameValue gameValue = 0;

	// If game is finished, set value to +- max
	if(coordinator->GetGameState() == GameState::GAME_OVER)
	{
		if(coordinator->GetActivePlayer() == playerIndex)
		{
			return winValue;
		}
		else
		{
			return -winValue;
		}
	}

	// Evaluate if you can draw again
	if(coordinator->GetGameState() == GameState::REMOVE)
	{
		if(coordinator->GetActivePlayer() == playerIndex)
		{
			gameValue += turnValue;
		}
		else
		{
			gameValue -= turnValue;
		}
	}

	// Evaluate board
	const GameBoard* board = coordinator->GetGameBoardPointer();

	std::vector<int> enemyIndices;
	// check for all tokens and all moves players can do (more = better)
	for(auto pos = 0; pos < board->GetSize(); ++pos)
	{
		if( board->GetValue(pos) == playerIndex)
		{
			gameValue += tokenValue;

			GameValue NumMoves = static_cast<GameValue>(board->GetPossibleMoves(pos).size());
			gameValue += NumMoves * moveValue;
		}
		else if(board->GetValue(pos) != 0)
		{
			int enemyIndex = board->GetValue(pos);
			if (std::find(std::begin(enemyIndices), std::end(enemyIndices), enemyIndex) == std::end(enemyIndices))
			{
				enemyIndices.push_back(enemyIndex);
			}

			gameValue -= tokenValue;

			GameValue NumMoves = static_cast<GameValue>(board->GetPossibleMoves(pos).size());
			gameValue -= NumMoves * moveValue;
		}
	}

	GameValue NumMills = static_cast<GameValue>(board->GetMillsOfPlayer(playerIndex).size());
	gameValue += NumMills * millValue;

	for(auto enemyIndex : enemyIndices)
	{
		NumMills = static_cast<GameValue>(board->GetMillsOfPlayer(enemyIndex).size());
		gameValue -= NumMills * millEnemyValue;
	}

	return gameValue;
}

std::vector<AIBacktrack::GameValue>
AIBacktrack::Draw_TrueBacktrack_Evaluator(GameCoordinatorBase* coordinator, const std::vector<int>& playerIndices, int BacktrackDepth) const
{
	if(BacktrackDepth == 0)
	{
		// careful, this return value has "dummy" values, as it's an implicit map between playerindex and gamevalue
		std::vector<GameValue> values;
		for( int i = static_cast<int>(playerIndices.size())-1; i >= 0; --i)
		{
			if(playerIndices[i] >= values.size())
			{
				values.resize(playerIndices[i]+1,std::numeric_limits<GameValue>::lowest());
			}
			// this creates an implicit map of [playerIndex -> GameValue]
			values[playerIndices[i]] = CalculateGameValue(coordinator, playerIndices[i]);
		}
		return values;
	}

	// --------------------------------------------
	// responsiveness fix here
	// just call processEvents from time to time
	// it's not a nice solution but is easy to implement given little time
	// testing also gave a negligible performance loss
	if(BacktrackDepth > 2)
	{
		qApp->processEvents();
	}
	// --------------------------------------------

	int playerIndex = coordinator->GetActivePlayer();
	std::vector<GameValue> targetValues(playerIndices.size(),std::numeric_limits<GameValue>::lowest());
	for( int i = static_cast<int>(playerIndices.size())-1; i >= 0; --i)
	{
		if(playerIndices[i] >= targetValues.size())
		{
			targetValues.resize(playerIndices[i]+1,std::numeric_limits<GameValue>::lowest());
		}
	}
	const std::vector<DrawAction> draws = coordinator->GetPossibleDraws();

	if(draws.empty())
	{
		// In case of Game Over return +- winValue for each player
		for(int i = 0; i < playerIndices.size(); ++i)
		{
			if(coordinator->GetActivePlayer() == playerIndices[i])
			{
				targetValues[playerIndices[i]] = winValue;
			}
			else
			{
				targetValues[playerIndices[i]] = - winValue;
			}
		}
	}
	else
	{
		for(const auto& draw : draws)
		{
			// Do draw, evaluate game and undo draw again
			coordinator->DoDrawAction(draw);

			auto valueVec = Draw_TrueBacktrack_Evaluator(coordinator, playerIndices, BacktrackDepth-1);

			coordinator->UndoPreviousDraw();

			// save best value for normal cases
			if(valueVec[playerIndex] > targetValues[playerIndex])
			{
				if(valueVec[playerIndex] >= winValue)
				{
					// break in case a winning strategy is found -> saves some calculation time
					return valueVec;
				}
				targetValues = valueVec;
			}
		}
	}

	return targetValues;
}

DrawAction AIBacktrack::Draw_TrueBacktrack(GameCoordinatorBase* coordinator, int BacktrackDepth) const
{
	const int curPlayerIndex = coordinator->GetActivePlayer();

	const std::vector<DrawAction> draws = coordinator->GetPossibleDraws();
	std::vector<GameValue> values(draws.size(),std::numeric_limits<GameValue>::lowest());

	std::vector<int> playerIndices;
	for(int i=coordinator->GetMinPlayerIndex(); i<=coordinator->GetMaxPlayerIndex(); ++i)
	{
		playerIndices.push_back(i);
	}

	bool firstTurn = (coordinator->GetTokensPlaced(curPlayerIndex) == 0);
	if(firstTurn)
	{
		// Cut depth at start of game, as result is the same
		// (placing in middles is always best)
		BacktrackDepth = std::min(BacktrackDepth,3);
	}

	volatile bool foundWinStrategy = false;
	#pragma omp parallel default(none) shared(values, foundWinStrategy) firstprivate(draws, coordinator, curPlayerIndex, BacktrackDepth, playerIndices)
	{
		GameCoordinatorBase coordinatorCopy = *coordinator;
		#pragma omp for
		for(auto i = 0; i<draws.size(); ++i)
		{
			// Pseudo-Break, as OpenMP doesn't allow true breaks
			if(foundWinStrategy) continue;

			coordinatorCopy.DoDrawAction(draws[i]);

			auto valueVec = Draw_TrueBacktrack_Evaluator(&coordinatorCopy, playerIndices, BacktrackDepth);

			if(valueVec[curPlayerIndex] >= winValue)
			{
				// Stop searching, but don't break since openMP forbids it
				// each thread will just skip next iterations
				foundWinStrategy = true;
			}

			// Reset board state
			coordinatorCopy.UndoPreviousDraw();

			#pragma omp critical
			{
				values[i] = valueVec[curPlayerIndex];
			}
		}
	}

	std::random_device rd;
	std::mt19937 rngGen(rd());

	GameValue targetVal = std::numeric_limits<GameValue>::lowest();
	int targetIndex = -1;
	for(auto i = 0; i<values.size(); ++i)
	{
		if(values[i] > targetVal)
		{
			targetVal = values[i];
			targetIndex = i;
		}
		else if(values[i] == targetVal)
		{
			// if several turns lead to same GameValue, choose one randomly
			// (so the ai doesn't feel as repetitive)
			std::uniform_int_distribution<int> rngDistr(0,1); // 0 or 1
			if(rngDistr(rngGen) == 0)
			{
				targetVal = values[i];
				targetIndex = i;
			}
		}

	}

	return draws[targetIndex];
}

// --------------------------------------------------
// Deprectaed AIs below
// --------------------------------------------------

// Deprecated version of AI
DrawAction AIBacktrack::Draw_PseudoBacktrack(GameCoordinatorBase* coordinator, int BacktrackDepth) const
{
	DrawAction targetDraw;

	std::random_device rd;
	std::mt19937 rngGen(rd());

	auto curPlayerIndex = coordinator->GetActivePlayer();
	GameValue targetVal = std::numeric_limits<GameValue>::lowest();

	std::vector<DrawAction> draws = coordinator->GetPossibleDraws();
	for(const auto& draw : draws)
	{
		coordinator->DoDrawAction(draw);

		GameValue val = -winValue;
		if(BacktrackDepth > 0)
		{
			// Do optimal turn for next active player now, and see what's the best outcome for current active player
			DrawAction nextDraw = Draw_PseudoBacktrack(coordinator, BacktrackDepth-1);

			coordinator->DoDrawAction(nextDraw);

			val = CalculateGameValue(coordinator, curPlayerIndex);

			// Reset board state
			coordinator->UndoPreviousDraw();
		}
		else
		{
			val = CalculateGameValue(coordinator, curPlayerIndex);
		}

		// Reset board state
		coordinator->UndoPreviousDraw();

		if(val > targetVal)
		{
			targetVal = val;
			targetDraw = draw;
		}
		else if(val == targetVal)
		{
			std::uniform_int_distribution<int> rngDistr(0,1); // 0 or 1
			if(rngDistr(rngGen) == 0)
			{
				targetVal = val;
				targetDraw = draw;
			}
		}
	}

	if(targetDraw.type == DrawType::NONE)
	{
		return DrawDefault(coordinator->GetGameBoardPointer(),curPlayerIndex,coordinator->GetGameState());
	}
	else{
		// valid draw determined
		return targetDraw;
	}
}

// Deprecated Version of AI
AIBacktrack::GameValue AIBacktrack::Draw_TrueBacktrack_Evaluator_MinimizingEnemy(GameCoordinatorBase* coordinator, int playerIndex, int BacktrackDepth) const
{
	if(BacktrackDepth == 0)
	{
		return CalculateGameValue(coordinator, playerIndex);
	}

	bool ownDraw = ( playerIndex == coordinator->GetActivePlayer() );
	GameValue targetVal = std::numeric_limits<GameValue>::lowest();
	if(!ownDraw)
	{
		targetVal = std::numeric_limits<GameValue>::max();
	}
	std::vector<DrawAction> draws = coordinator->GetPossibleDraws();
	for(const auto& draw : draws)
	{
		// Do draw, evaluate game and undo draw again
		coordinator->DoDrawAction(draw);

		GameValue val = Draw_TrueBacktrack_Evaluator_MinimizingEnemy(coordinator, playerIndex, BacktrackDepth-1);

		coordinator->UndoPreviousDraw();

		// save best value
		if(val > targetVal && ownDraw)
		{
			targetVal = val;
		}
		else if(val < targetVal && !ownDraw)
		{
			targetVal = val;
		}
	}
	return targetVal;
}

// Deprecated Version of AI
DrawAction AIBacktrack::Draw_TrueBacktrack_MinimizingEnemy(GameCoordinatorBase* coordinator, int BacktrackDepth) const
{
	const int curPlayerIndex = coordinator->GetActivePlayer();

	std::vector<DrawAction> draws = coordinator->GetPossibleDraws();
	std::vector<GameValue> values(draws.size());

	#pragma omp parallel default(none) shared(values) firstprivate(draws, coordinator, curPlayerIndex, BacktrackDepth)
	{
		GameCoordinatorBase coordinatorCopy = *coordinator;
		#pragma omp for
		for(auto i = 0; i<draws.size(); ++i)
		{
			DrawAction draw = draws[i];

			coordinatorCopy.DoDrawAction(draw);

			GameValue val = Draw_TrueBacktrack_Evaluator_MinimizingEnemy(&coordinatorCopy, curPlayerIndex, BacktrackDepth);

			// Reset board state
			coordinatorCopy.UndoPreviousDraw();

			#pragma omp critical
			{
				values[i] = val;
			}
		}
	}

	std::random_device rd;
	std::mt19937 rngGen(rd());

	GameValue targetVal = std::numeric_limits<GameValue>::lowest();
	int targetIndex = -1;
	for(auto i = 0; i<values.size(); ++i)
	{
		if(values[i] > targetVal)
		{
			targetVal = values[i];
			targetIndex = i;
		}
		else if(values[i] == targetVal)
		{
			std::uniform_int_distribution<int> rngDistr(0,1); // 0 or 1
			if(rngDistr(rngGen) == 0)
			{
				targetVal = values[i];
				targetIndex = i;
			}
		}

	}
	return draws[targetIndex];
}

}
}
