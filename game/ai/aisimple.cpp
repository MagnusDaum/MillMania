#include "aisimple.h"

#include <algorithm>
#include <random>

namespace MillGame
{
namespace AI
{

DrawAction AISimple::DetermineDraw(const GameCoordinatorBase *coordinator, int difficulty) const
{
	DrawAction draw;

	const GameBoard* board = coordinator->GetGameBoardPointer();
	int playerIndex = coordinator->GetActivePlayer();
	GameState gameState = coordinator->GetGameState();
	if ( difficulty == 1 ) draw = Draw_PreventMills(board, playerIndex, gameState);
	else if ( difficulty > 1 ) draw = Draw_CloseMills(board, playerIndex, gameState);
	else draw = DrawDefault(board, playerIndex, gameState);

	return draw;
}


DrawAction AISimple::DrawDefault(const GameBoard *board, int playerIndex, GameState gameState, const std::vector<int>& excludePositions) const
{
	DrawAction draw;

	std::random_device rd;
	std::mt19937 rngGen(rd());

	switch(gameState)
	{
	case GameState::PLACE:
		{
			// find random position which is still unoccupied
			std::uniform_int_distribution<int> rngDistr(0,static_cast<int>(board->GetSize())-1);
			int position = rngDistr(rngGen);
			while(board->GetValue(position) != 0
				  && std::find(std::begin(excludePositions), std::end(excludePositions), position) == std::end(excludePositions))
			{
				position = rngDistr(rngGen);
			}
			draw = DrawAction(DrawType::PLACE,position);
		}
		break;
	case GameState::MOVE:
		{
			bool canJump = board->GetTokenCount(playerIndex) == board->minTokenCount;
			// find random position with correct token
			std::vector<int> tokenPositions;
			for(auto pos=0; pos<board->GetSize(); ++pos)
			{
				if( board->GetValue(pos) == playerIndex
					&& ( !board->GetPossibleMoves(pos, canJump).empty() )
					&& std::find(std::begin(excludePositions), std::end(excludePositions), pos) == std::end(excludePositions))
				{
					tokenPositions.push_back(pos);
				}
			}
			if(tokenPositions.empty())
			{
				for(auto pos=0; pos<board->GetSize(); ++pos)
				{
					if( board->GetValue(pos) == playerIndex && !board->GetPossibleMoves(pos, canJump).empty() )
					{
						tokenPositions.push_back(pos);
					}
				}
			}
			std::uniform_int_distribution<int> rngDistr(0,static_cast<int>(tokenPositions.size())-1);
			int position = tokenPositions[rngDistr(rngGen)];

			// find random move from this position
			auto moves = board->GetPossibleMoves(position, canJump);
			rngDistr = std::uniform_int_distribution<int>(0,static_cast<int>(moves.size())-1);
			int newPos = moves[rngDistr(rngGen)];

			draw = DrawAction( DrawType::MOVE, position, newPos);
		}
		break;
	case GameState::REMOVE:
		{
			// find random position which is occupied with enemy player
			std::uniform_int_distribution<int> rngDistr(0,static_cast<int>(board->GetSize())-1);
			int position = rngDistr(rngGen);
			while( !board->IsRemovePossible(playerIndex,position)
				   && std::find(std::begin(excludePositions), std::end(excludePositions), position) == std::end(excludePositions) )
			{
				position = rngDistr(rngGen);
			}
			draw = DrawAction(DrawType::REMOVE,position);
		}
		break;
	default: break;
	}

	return draw;
}

DrawAction AISimple::Draw_PreventMills(const GameBoard *board, int playerIndex, GameState gameState) const
{
	DrawAction draw;

	switch(gameState)
	{
	case GameState::PLACE:
		{
			bool moveFound = false;
			int position = 0;
			for(; position < board->GetSize(); ++position)
			{
				if( board->GetValue(position) == 0 && CheckIfPreventsMill(board, playerIndex, position))
				{
					moveFound = true;
					break;
				}
			}
			if(moveFound)
			{
				draw = DrawAction(DrawType::PLACE,position);
			}
			else
			{
				draw = DrawDefault(board, playerIndex, gameState);
			}
			break;
		}
	case GameState::MOVE:
		{
			bool canJump = board->GetTokenCount(playerIndex) == board->minTokenCount;
			int newPosition = -1;
			int position = 0;
			std::vector<int> stayPositions; // don't leave positions whcih already prevent an enemy mill
			for(; position < board->GetSize(); ++position)
			{
				if( board->GetValue(position) == playerIndex)
				{
					// check all moves
					auto moves = board->GetPossibleMoves(position, canJump);
					for( auto newPos : moves)
					{
						if(CheckIfPreventsMill(board, playerIndex, newPos, position))
						{
							newPosition = newPos;
							break;
						}
					}
					if(CheckIfPreventsMill(board, playerIndex, position))
					{
						stayPositions.push_back(position);
					}
				}
				if(newPosition >= 0) break;
			}
			if(newPosition >= 0)
			{
				draw = DrawAction(DrawType::MOVE, position, newPosition);
			}
			else
			{
				if(stayPositions.empty())
				{
					draw = DrawDefault(board, playerIndex, gameState);
				}
				else
				{
					draw = DrawDefault(board, playerIndex, gameState, stayPositions);
				}
			}
			break;
		}
	default:
		draw = DrawDefault(board, playerIndex, gameState);
	}

	return draw;
}

DrawAction AISimple::Draw_CloseMills(const GameBoard *board, int playerIndex, GameState gameState) const
{
	DrawAction draw;

	switch(gameState)
	{
	case GameState::PLACE:
		{
			bool moveFound = false;
			int position = 0;
			for(; position < board->GetSize(); ++position)
			{
				if( board->GetValue(position) == 0 && CheckIfClosesMill(board, playerIndex, position))
				{
					moveFound = true;
					break;
				}
			}
			if(moveFound)
			{
				draw = DrawAction(DrawType::PLACE,position);
			}
			else
			{
				draw = Draw_PreventMills(board, playerIndex, gameState);
			}
			break;
		}
	case GameState::MOVE:
		{
			bool canJump = board->GetTokenCount(playerIndex) == board->minTokenCount;
			int newPosition = -1;
			int position = 0;
			for(; position < board->GetSize(); ++position)
			{
				if( board->GetValue(position) == playerIndex )
				{
					// check all moves
					auto moves = board->GetPossibleMoves(position, canJump);
					for( auto newPos : moves)
					{
						if(CheckIfClosesMill(board, playerIndex, newPos, position))
						{
							newPosition = newPos;
							break;
						}
					}
				}
				if(newPosition >= 0) break;
			}
			if(newPosition >= 0)
			{
				draw = DrawAction(DrawType::MOVE, position, newPosition);
			}
			else
			{
				draw = Draw_PreventMills(board, playerIndex, gameState);
			}
			break;
		}
	default:
		draw = Draw_PreventMills(board, playerIndex, gameState);
	}

	return draw;
}


bool AISimple::CheckIfClosesMill(const GameBoard *board, int playerIndex, int newPos, int oldPos) const
{
	if(board->GetValue(newPos) == 0)
	{
		//check if placement would close a mill
		auto mills = board->GetAllPossibleMills(newPos);
		for(auto &m : mills)
		{
			bool foundMill = true;
			for(const auto& mpos : m)
			{
				if( ! ( mpos == newPos
						|| ( board->GetValue(mpos) == playerIndex
							 && (mpos != oldPos || oldPos < 0 ) ) ) )
				{
					foundMill = false;
					break;
				}
			}
			if(foundMill)
			{
				return true;
			}
		}
	}

	return false;
}

bool AISimple::CheckIfPreventsMill(const GameBoard *board, int playerIndex, int newPos, int oldPos) const
{
	//check if placement would prevent a mill
	auto mills = board->GetAllPossibleMills(newPos);
	for(const auto &mill : mills)
	{
		// search in possible mills for any which is lacking only one token at newPos
		bool foundPossibleMill = true;
		int plIdx = -1;
		for(auto mill_pos : mill)
		{
			if( mill_pos == newPos
				|| ( board->GetValue(mill_pos) != playerIndex
					 && board->GetValue(mill_pos) > 0
					 && ( mill_pos != oldPos || oldPos < 0 )
					 && ( board->GetValue(mill_pos) == plIdx || plIdx <0 ) ) )
			{
				if(plIdx < 0 && board->GetValue(mill_pos) > 0)
				{
					plIdx = board->GetValue(mill_pos);
				}
			}
			else
			{
				foundPossibleMill = false;
				break;
			}
		}
		if(foundPossibleMill)
		{
			//Check if mill is closeable by enemy
			bool millCloseable = false;
			if(oldPos < 0)
			{
				// mill is always closeable by placing
				millCloseable = true;
			}
			else
			{
				// look around newPos to make sure neighbors are actually enemy players
				auto neighbors = board->GetNeighbors(newPos);
				for(auto neighbor : neighbors)
				{
					// check if neighbor is enemy and not yet part of found mill
					if(board->GetValue(neighbor) == plIdx
						&& std::find(std::begin(mill), std::end(mill), neighbor) == std::end(mill) )
					{
						millCloseable = true;
						break;
					}
				}
			}

			if(millCloseable)
			{
				return true;
			}
		}
	}

	return false;
}

}
}
