#include "aibase.h"

#include <algorithm>
#include <random>

namespace MillGame
{

namespace AI
{

DrawAction AIBase::DrawDefault(const GameBoard *board, int playerIndex, GameState gameState) const
{
	DrawAction draw;

	std::random_device rd;
	std::mt19937 rngGen(rd());

	switch(gameState)
	{
	case GameState::PLACE:
		{
			// find random position which is still unoccupied
			std::uniform_int_distribution<int> rngDistr(0,static_cast<int>(board->GetSize())-1);
			int position = rngDistr(rngGen);
			while(board->GetValue(position) != 0)
			{
				position = rngDistr(rngGen);
			}
			draw = DrawAction(DrawType::PLACE,position);
		}
		break;
	case GameState::MOVE:
		{
			bool canJump = board->GetTokenCount(playerIndex) == board->minTokenCount;
			// find random position with correct token
			std::vector<int> tokenPositions;
			for(auto pos=0; pos<board->GetSize(); ++pos)
			{
				if( board->GetValue(pos) == playerIndex
					&& ( !board->GetPossibleMoves(pos, canJump).empty() ) )
				{
					tokenPositions.push_back(pos);
				}
			}
			if(tokenPositions.empty())
			{
				for(auto pos=0; pos<board->GetSize(); ++pos)
				{
					if( board->GetValue(pos) == playerIndex && !board->GetPossibleMoves(pos, canJump).empty() )
					{
						tokenPositions.push_back(pos);
					}
				}
			}
			std::uniform_int_distribution<int> rngDistr(0,static_cast<int>(tokenPositions.size())-1);
			int position = tokenPositions[rngDistr(rngGen)];

			// find random move from this position
			auto moves = board->GetPossibleMoves(position, canJump);
			rngDistr = std::uniform_int_distribution<int>(0,static_cast<int>(moves.size())-1);
			int newPos = moves[rngDistr(rngGen)];

			draw = DrawAction( DrawType::MOVE, position, newPos);
		}
		break;
	case GameState::REMOVE:
		{
			// find random position which is occupied with enemy player
			std::uniform_int_distribution<int> rngDistr(0,static_cast<int>(board->GetSize())-1);
			int position = rngDistr(rngGen);
			while( !board->IsRemovePossible(playerIndex,position) )
			{
				position = rngDistr(rngGen);
			}
			draw = DrawAction(DrawType::REMOVE,position);
		}
		break;
	}

	return draw;
}

}
}
