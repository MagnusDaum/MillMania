#ifndef AISIMPLE_H
#define AISIMPLE_H

#include <vector>

#include "aibase.h"

namespace MillGame
{

namespace AI
{

class AISimple : AIBase
{
public:
	// "main function", this is called by game engine
	DrawAction DetermineDraw(const GameCoordinatorBase *coordinator, int difficulty = 0) const;

private:
	// places tokens randomly
	DrawAction DrawDefault(const GameBoard *board, int playerIndex, GameState gameState, const std::vector<int>& excludePositions = std::vector<int>()) const;

	// denies enemy mills or calls default ai
	DrawAction Draw_PreventMills(const GameBoard *board, int playerIndex, GameState gameState) const;

	// closes own mills or calls PreventMills ai
	DrawAction Draw_CloseMills(const GameBoard *board, int playerIndex, GameState gameState) const;

	// checks if draw closes a mill
	bool CheckIfClosesMill(const GameBoard *board, int playerIndex, int newPos, int oldPos = -1) const;

	// checks if draw prevents a mill
	bool CheckIfPreventsMill(const GameBoard *board, int playerIndex, int newPos, int oldPos = -1) const;
};

}
}

#endif // AISIMPLE_H
