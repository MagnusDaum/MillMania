#ifndef AICOORDINATOR_H
#define AICOORDINATOR_H

#include "gamecoordinatorbase.h"

//AIs
#include "ai/aisimple.h"
#include "ai/aibacktrack.h"

namespace MillGame
{

class AICoordinator
{

public:
	AICoordinator() = default;
	~AICoordinator() = default;

	// This function is called by game engine
	// call AIs' DetermineDraw function according to aiType
	DrawAction DetermineDraw(const GameCoordinatorBase *coordinator, int aiType = 0, int aiDifficulty = 0) const;

private:
	AI::AISimple aisimple;
	AI::AIBacktrack aibacktrack;

};

}

#endif // AICOORDINATOR_H
