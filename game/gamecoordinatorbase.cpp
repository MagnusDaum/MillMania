#include "GameCoordinatorBase.h"

#include <numeric>

namespace MillGame
{

GameCoordinatorBase::GameCoordinatorBase(int positionsPerBoardLayer, GameMode gameMode, int startPlayer)
	: board(positionsPerBoardLayer)
	, gameMode(gameMode)
	, tokensPlacedPerPlayer(maxPlayerIndex+1, 0)
{
	ResetGame(startPlayer);
}

void GameCoordinatorBase::ResetGame(int startPlayer)
{
	board.Reset();
	previousTurns.clear();

	gameState = GameState::PLACE;
	tokensPlacedPerPlayer.clear();
	tokensPlacedPerPlayer.resize(maxPlayerIndex+1, 0);
	activePlayer = std::min(std::max(startPlayer, minPlayerIndex),maxPlayerIndex);
}

void GameCoordinatorBase::ChangeGameMode(GameMode gameMode)
{
	this->gameMode = gameMode;
	ResetGame(minPlayerIndex);
}

bool GameCoordinatorBase::DoDrawAction(DrawAction draw)
{
	// assert valid draw
	if( draw.position < 0 || draw.position > board.GetSize() ) return false;

	if( draw.type == DrawType::MOVE && (draw.newPosition < 0 || draw.newPosition > board.GetSize())) return false;

	if( draw.type == DrawType::REMOVE && gameState != GameState::REMOVE) return false;
	if( draw.type == DrawType::PLACE && gameState != GameState::PLACE) return false;
	if( draw.type == DrawType::MOVE && gameState != GameState::MOVE
				&& !(gameState == GameState::PLACE && gameMode == GameMode::LASKER)) return false;

	// now do draw and update player and game state accordingly
	bool successful = false;
	bool millWasCreated = false;

	GameState curGameState = gameState;
	int curPlayer = activePlayer;
	auto curTurnHistoryLength = previousTurns.size();

	auto res = board.DoDrawAction(activePlayer, draw);
	if(res.first)
	{
		// this should happen every time, but still make this check just to be sure
		previousTurns.insert(previousTurns.begin()+curTurnHistoryLength, TurnState(draw, curGameState, curPlayer));
		if(draw.type == DrawType::PLACE)
		{
			tokensPlacedPerPlayer[curPlayer]++;
		}
		else if(gameState == GameState::REMOVE)
		{
			gameState = GameState::PLACE; // reset gameState -> will be set to 1 by DetermineGameState() if necessary
		}
		millWasCreated = res.second;
		successful = true;
	}

	if(successful)
	{
		if(millWasCreated)
		{
			gameState = GameState::REMOVE;
		}

		// this will set the game state accordingly for differencing between placing and moving
		DetermineGameState(gameState);
		if (gameState == GameState::PLACE || gameState == GameState::MOVE)
		{
			SwitchPlayer();
		}
	}

	return successful;
}


void GameCoordinatorBase::UndoPreviousDraw()
{
	// Careful, undoing a remove only works for two players
	// as tokens are not saved, and thus it's only for two players implicitly known which token was removed
	if(!previousTurns.empty())
	{

		DrawAction previousDraw = previousTurns.back().draw;
		GameState previousGameState = previousTurns.back().gameState;
		int previousPlayer = previousTurns.back().activePlayer;
		previousTurns.pop_back();

		if(gameState == GameState::GAME_OVER && previousDraw.type == DrawType::REMOVE)
		{
			// Since player doesn't change after remove in case of game over
			// go two further back to replace correct token
			if(previousTurns.size() > 1)
			{
				activePlayer = previousTurns.at(previousTurns.size()-2).activePlayer;
			}
		}

		board.UndoDrawAction(activePlayer, previousDraw);

		if(previousDraw.type == DrawType::PLACE)
		{
			tokensPlacedPerPlayer[previousPlayer]--;
		}

		gameState = previousGameState;
		activePlayer = previousPlayer;
	}
}

bool GameCoordinatorBase::CanMoveToken(int position) const
{
	// a token can be moved, if it belongs to activePlayer
	// and game is in MOVE mode and there is a free field nearby
	int playerId = board.GetValue(position);
	return ( (gameState == GameState::MOVE || gameMode == GameMode::LASKER)
			&& playerId == activePlayer
			&& ( !board.GetPossibleMoves(position, CanPlayerJump(playerId)).empty() )
			);
}

bool GameCoordinatorBase::CanPlayerJump(int playerIndex) const
{
	// a player can jump if he has minimum token count
	return board.GetTokenCount(playerIndex) == board.minTokenCount;
}

std::vector<DrawAction> GameCoordinatorBase::GetPossibleDraws() const
{
	std::vector<DrawAction> draws;

	// Removes
	if(gameState == GameState::REMOVE)
	{
		auto removes = board.GetPossibleRemoves(activePlayer);
		for(const auto& remove : removes)
		{
			draws.push_back(DrawAction(DrawType::REMOVE,remove));
		}
	}

	// Placements
	if ( gameState == GameState::PLACE)
	{
		auto placements = board.GetPossiblePlacements();
		for(const auto& placement : placements)
		{
			draws.push_back(DrawAction(DrawType::PLACE,placement));
		}
	}

	// Movements
	if( gameState == GameState::MOVE || gameState == GameState::PLACE && gameMode == GameMode::LASKER)
	{
		bool canJump = CanPlayerJump(activePlayer);
		for(auto pos=0; pos<board.GetSize(); ++pos)
		{
			if(board.GetValue(pos) == activePlayer)
			{
				auto moves = board.GetPossibleMoves(pos, canJump);
				for(auto i=0; i<moves.size(); ++i)
				{
					draws.push_back(DrawAction(DrawType::MOVE,pos,moves[i]));
				}
			}
		}
	}

	return draws;
}

int GameCoordinatorBase::GetBoardValue(int position) const
{
	return board.GetValue(position);
}

std::size_t GameCoordinatorBase::GetBoardSize() const
{
	return board.GetSize();
}

std::size_t GameCoordinatorBase::GetNumberOfTurns() const
{
	return previousTurns.size();
}

int GameCoordinatorBase::GetRemainingTokensToPlace(int playerIndex) const
{
	return board.maxTokenCount - tokensPlacedPerPlayer.at(playerIndex);
}

int GameCoordinatorBase::GetTokensPlaced(int playerIndex) const
{
	return tokensPlacedPerPlayer.at(playerIndex);
}

int GameCoordinatorBase::GetActivePlayer() const
{
	return activePlayer;
}

GameState GameCoordinatorBase::GetGameState() const
{
	return gameState;
}

int GameCoordinatorBase::GetPlayerCount() const
{
	return playerCount;
}

const GameBoard* GameCoordinatorBase::GetGameBoardPointer() const
{
	return &board;
}

int GameCoordinatorBase::GetMinPlayerIndex() const
{
	return minPlayerIndex;
}

int GameCoordinatorBase::GetMaxPlayerIndex() const
{
	return maxPlayerIndex;
}

void GameCoordinatorBase::SwitchPlayer(bool forwards)
{
	if(forwards)
	{
		if(++activePlayer > maxPlayerIndex)
		{
			activePlayer = minPlayerIndex;
		}
	}
	else
	{
		if(--activePlayer < minPlayerIndex)
		{
			activePlayer = maxPlayerIndex;
		}
	}
}

void GameCoordinatorBase::DetermineGameState(GameState& gameState)
{
	if(gameState == GameState::PLACE || gameState == GameState::MOVE)
	{
		bool noMorePlacements = true;
		for(auto i = minPlayerIndex; i <= maxPlayerIndex; ++i)
		{
			if(tokensPlacedPerPlayer[i] < board.maxTokenCount)
			{
				noMorePlacements = false;
				break;
			}
		}
		if(noMorePlacements)
		{
			gameState = GameState::MOVE;
		}
		if(gameState == GameState::MOVE)
		{
			for(auto i=minPlayerIndex; i<=maxPlayerIndex; ++i)
			{
				if( !board.IsMovesPossibleForPlayer(i) )
				{
					gameState = GameState::GAME_OVER;
					break;
				}
			}
		}
	}
	// else: nothing changes
}

}
