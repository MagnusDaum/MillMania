#include "gameboard.h"

#include <algorithm>

namespace MillGame
{

GameBoard::GameBoard(int positionsPerPolygon)
	: positionsPerPolygon(positionsPerPolygon)
	, boardSize(NumPolygons * positionsPerPolygon)
	, maxTokenCount( 1 + (NumPolygons-1) * positionsPerPolygon / 2 )
	, minTokenCount(NumPolygons)
	, board(boardSize)
{
	FillMoveMap();
	FillMillVectors();
	Reset();
}

void GameBoard::Reset()
{
	for(auto i=0; i<board.size(); ++i)
	{
		board[i] = 0;
	}
}

void GameBoard::FillMoveMap()
{
	MoveMap.clear();
	for(auto position = 0; position < boardSize; ++position)
	{
		std::vector<int> moves;
		if( position % 2 == 0)
		{
			// even positions
			if (position % positionsPerPolygon == 0)
			{
				moves.push_back(position-1+positionsPerPolygon);
			}
			else
			{
				moves.push_back(position-1);
			}

			moves.push_back(position+1);
		}
		else
		{
			// odd positions
			moves.push_back(position-1);

			if ((position+1) % positionsPerPolygon == 0)
			{
				moves.push_back(position+1-positionsPerPolygon);
			}
			else
			{
				moves.push_back(position+1);
			}

			if(position-positionsPerPolygon >= 0)
			{
				moves.push_back(position-positionsPerPolygon);
			}

			if(position+positionsPerPolygon < boardSize)
			{
				moves.push_back(position+positionsPerPolygon);
			}
		}
		MoveMap[position] = moves;
	}
}

void GameBoard::FillMillVectors()
{
	MillsPerPositionVec.clear();
	MillVec.clear();
	for(auto position = 0; position < boardSize; ++position)
	{
		std::vector< std::array<int, NumPolygons> > mills;

		std::array<int, NumPolygons> mill;
		if( position % 2 == 0)
		{
			// even positions
			// position is always start or end of mill

			mill[0] = position;
			for(auto i=1; i<NumPolygons; ++i)
			{
				if(position % positionsPerPolygon == 0)
				{
					mill[i] = position + positionsPerPolygon - i;
				}
				else
				{
					mill[i] = position - i;
				}
			}
			std::sort(mill.begin(),mill.end());
			mills.push_back(mill);

			mill[0] = position;
			for(auto i=1; i<NumPolygons; ++i)
			{
				if((position+i) % positionsPerPolygon == 0)
				{
					mill[i] = position - positionsPerPolygon + i;
				}
				else
				{
					mill[i] = position + i;
				}
			}
			std::sort(mill.begin(),mill.end());
			mills.push_back(mill);
		}
		else
		{
			// odd positions
			int millIdx = 0;
			for(auto i=-NumPolygons+1; i<NumPolygons; ++i)
			{
				if(position + i*positionsPerPolygon >= 0 && position + i*positionsPerPolygon <= boardSize)
				{
					mill[millIdx++] = position + i*positionsPerPolygon;
				}
			}
			std::sort(mill.begin(),mill.end());
			mills.push_back(mill);

			millIdx = 0;
			for(auto i=-NumPolygons/2; i<NumPolygons/2+1; ++i)
			{
				if( i > 0 &&  (position+i) % positionsPerPolygon == 0)
				{
					mill[millIdx++] = position - positionsPerPolygon + i;
				}
				else
				{
					mill[millIdx++] = position + i;
				}
			}
			std::sort(mill.begin(),mill.end());
			mills.push_back(mill);
		}
		MillsPerPositionVec.push_back(mills);

		for(auto i = 0; i < mills.size(); ++i)
		{
			bool addMill = true;
			for(auto k = 0; k < MillVec.size(); ++k)
			{
				if(MillVec[k] == mills[i]) // std::array has == operator and mills are sorted
				{
					addMill = false;
					break;
				}
			}
			if(addMill)
			{
				MillVec.push_back(mills[i]);
			}
		}
	}
}

std::pair<bool,bool> GameBoard::DoDrawAction(int player, DrawAction draw)
{
	switch(draw.type)
	{
	case 0: return PlaceToken(player, draw.position); break;
	case 1: return MoveToken(player, draw.position, draw.newPosition); break;
	case 2: return std::make_pair(RemoveToken(player, draw.position), false); break;
	default: return std::make_pair(false, false);
	}
}

void GameBoard::UndoDrawAction(int previousPlayer, DrawAction draw)
{
	switch(draw.type)
	{
	case 0:
		board[draw.position] = 0;
		break;
	case 1:
		board[draw.position] = board[draw.newPosition];
		board[draw.newPosition] = 0;
		break;
	case 2:
		board[draw.position] = previousPlayer;
		break;
	}
}

std::pair<bool,bool> GameBoard::PlaceToken(int player, int position)
{
	std::pair<bool,bool> result = std::make_pair(false, false);

	if(board[position] == 0)
	{
		board[position] = player;


		result.second = IsPartOfMill(player, position);

		result.first = true;
	}

	return result;
}

std::pair<bool,bool> GameBoard::MoveToken(int player, int oldPos, int newPos)
{
	std::pair<bool,bool> result = std::make_pair(false, false);

	if(board[newPos] == 0 && board[oldPos] == player)
    {
		auto moves = GetPossibleMoves(oldPos, GetTokenCount(player) == minTokenCount);
		if( std::find(moves.begin(), moves.end(), newPos) != moves.end() )
        {
			board[newPos] = board[oldPos]; // = token
			board[oldPos] = 0;

			result.second = IsPartOfMill(player, newPos);

			result.first = true;
        }
    }

	return result;
}

bool GameBoard::RemoveToken(int player, int position)
{
	if(IsRemovePossible(player, position))
	{
		board[position] = 0;
		return true;
	}
	return false;
}

bool GameBoard::IsMovesPossibleForPlayer(int player) const
{
	int tokenCount = GetTokenCount(player);
	if(tokenCount < GameBoard::minTokenCount)
	{
		return false;
	}
	else if ( tokenCount == minTokenCount)
	{
		return true;
	}

	for(auto i=0; i<board.size(); ++i)
	{
		if(board[i] == player)
		{
			if( !GetPossibleMoves(i).empty() )
			{
				return true;
			}
		}
	}
	return false;
}

bool GameBoard::IsPartOfMill(int player, int position) const
{
	bool isMill = false;
	if(board[position] == player)
	{
		auto mills = MillsPerPositionVec.at(position);
		for(auto millIdx = 0; millIdx < mills.size() && !isMill; ++millIdx)
		{
			isMill = true;
			for(auto k : mills[millIdx])
			{
				if(board[k] != player)
				{
					isMill = false;
					break;
				}
			}
		}
	}
	return isMill;
}

bool GameBoard::IsRemovePossible(int player, int position) const
{
	if(board[position] != 0 && board[position] != player)
	{
		if(IsPartOfMill(board[position], position))
		{
			// Check if every token is part of mill
			// If any other token is removeable -> current token is not removable as it's part of a mill
			bool anyRemovePossible = false;
			for(auto i=0; i<board.size(); ++i)
			{
				if(board[i] == board[position] && !IsPartOfMill(board[position], i))
				{
					anyRemovePossible = true;
					break;
				}
			}
			if(anyRemovePossible)
			{
				return false;
			}
		}
		return true;
	}
	return false;
}

std::vector<int> GameBoard::GetPossiblePlacements() const
{
	std::vector<int> placements;
	for(auto pos=0; pos<board.size(); ++pos)
	{
		if(board[pos] == 0)
		{
			placements.push_back(pos);
		}
	}
	return placements;
}

std::vector<int> GameBoard::GetPossibleMoves(int position, bool canJump) const
{
	std::vector<int> moves;
	if(canJump)
	{
		for(auto i = 0; i<board.size(); ++i)
		{
			if(board[i] == 0)
			{
				moves.push_back(i);
			}
		}
	}
	else
	{
		auto possibleMoves = MoveMap.at(position);
		for(auto move : possibleMoves)
		{
			if(board[move] == 0)
			{
				moves.push_back(move);
			}
		}
	}
	return moves;
}

std::vector<int> GameBoard::GetPossibleRemoves(int player) const
{
	std::vector<int> removes;
	for(auto pos=0; pos<board.size(); ++pos)
	{
		if(IsRemovePossible(player,pos))
		{
			removes.push_back(pos);
		}
	}
	return removes;
}


std::vector< std::array<int, GameBoard::NumPolygons> > GameBoard::GetAllPossibleMills(int position) const
{
	return MillsPerPositionVec.at(position);
}

std::vector<int> GameBoard::GetNeighbors(int position) const
{
	return MoveMap.at(position);
}

int GameBoard::GetValue(int position) const
{
    if( position < 0 || position > board.size() ) return -1; // assert valid values

    return board[position];
}

size_t GameBoard::GetSize() const
{
    return board.size();
}

int GameBoard::GetTokenCount(int player) const
{
	int count = 0;
	for(auto i=0; i<board.size(); ++i)
	{
		if(board[i] == player) count++;
	}
	return count;
}

std::vector< std::array<int, GameBoard::NumPolygons> > GameBoard::GetMillsOfPlayer(int player) const
{
	std::vector< std::array<int, NumPolygons> > mills;

	bool isMillForPlayer;
	for(auto i = 0; i < MillVec.size(); ++i)
	{
		isMillForPlayer = true;
		for(auto k = 0; k < MillVec[i].size(); ++k)
		{
			if(board[MillVec[i][k]] != player)
			{
				isMillForPlayer = false;
				break;
			}
		}
		if( isMillForPlayer )
		{
			mills.push_back(MillVec[i]);
		}
	}

//	for(auto pos=0; pos<board.size(); ++pos)
//	{
//		if(board[pos] == player)
//		{
//			auto pos_mills = GetAllPossibleMills(pos);
//			for(auto &pos_mill : pos_mills)
//			{
//				bool isMillForPlayer = true;
//				for(auto i : pos_mill)
//				{
//					if(board[i] != player)
//					{
//						isMillForPlayer = false;
//						break;
//					}
//				}
//				if ( isMillForPlayer && std::find(std::begin(mills), std::end(mills), pos_mill) == std::end(mills) )
//				{
//					mills.push_back(pos_mill);
//				}
//			}
//		}
//	}

	return mills;
}

}
