#ifndef GAMECOORDINATOR_H
#define GAMECOORDINATOR_H

#include "gamecoordinatorbase.h"
#include "aicoordinator.h"

namespace MillGame
{

/*
 * This is an extension of the base game engine
 * It enables usage of ai players, by calling specific AIs if activePlayer is not human
 *
 * Most overriden functions do exactly the same as the base engine,
 * except for checking if ai has to do next draw
 */
class GameCoordinator : public GameCoordinatorBase
{
public:
	GameCoordinator(int positionsPerBoardLayer, GameMode gameMode, int startPlayer);
	~GameCoordinator() = default;

	void ResetGame(int startPlayer);

	void ChangeGameMode(GameMode gameMode);

	// Checks if activePlayer is human before doing draw action
	// AI will be called internally
	bool RequestDrawAction(DrawAction draw);

	void UndoPreviousDraw();

	// Undoes several draws at once
	// this is necessary for returning to a human player, as ai would just draw again
	void UndoPreviousTurns(int NumberOfTurns = 1);

	bool ChangePlayerType(int playerIndex, int newPlayerType = -1, int newPlayerDifficulty = 0);

	int GetPlayerType(int index) const;

	bool GetIsAIFinished() const;

protected:
	using GameCoordinatorBase::DoDrawAction; // Inherit as private so you have to call RequestDrawAction from outside

	void SwitchPlayer(bool forwards = true);

	// this will repeatedly call AIs if necessary (i.e. ai vs ai game)
	void CallAIIfNecessary();

	virtual void DoAIDraw();

	// handling of ai type via AICoordinator
	AICoordinator aiCoordinator;

	std::vector<int> playerTypes; // 0 = human
	std::vector<int> playerDifficulties; // Only relevant for AI

	const int maxTurns = 500; // simple barrier so there is no endless standoff game (most relevant for ai vs ai game)

	// only do game changing actions if AI is not calculating
	bool isAIFinished = true;
};

}

#endif // GAMECOORDINATOR_H
