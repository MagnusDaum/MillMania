#include "aicoordinator.h"

#include "gameboard.h"

namespace MillGame
{

DrawAction AICoordinator::DetermineDraw(const GameCoordinatorBase *coordinator, int aiType, int aiDifficulty) const
{
	DrawAction draw;

	aiDifficulty = std::max(aiDifficulty, 0);

	// aiType == 0 -> human (should never happen)

	if(aiType == 1)
	{
		draw = aisimple.DetermineDraw(coordinator, aiDifficulty);
	}
	else if(aiType == 2)
	{
		draw = aibacktrack.DetermineDraw(coordinator, aiDifficulty);
	}
	else
	{
		draw = aisimple.DetermineDraw(coordinator, 0);
	}

	return draw;
}


}
