#ifndef DRAWACTION_H
#define DRAWACTION_H


namespace MillGame
{

enum class DrawType {NONE = -1, PLACE = 0, MOVE = 1, REMOVE = 2};

// This is the datatype describing an action a player can do
// type: place, move or remove
// position: where to place or remove, or where to move from
// newposition: where to move to (and irrelevant for placing and removing [i.e. -1])
struct DrawAction
{
	DrawAction() = default;
	DrawAction(DrawType type, int position) : type(type), position(position) {}
	DrawAction(DrawType type, int position, int newPosition): type(type), position(position), newPosition(newPosition) {}

	DrawType type = DrawType::NONE;
	int position = -1;
	int newPosition = -1;
};

}

#endif // DRAWACTION_H
